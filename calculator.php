<?php
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $numberOne = $_POST['number_1'];
      
        $numberTwo = $_POST['number_2'];
      
        $operationType = $_POST['operator'];
     
       if($operationType=="plus")
       {
           $result=$numberOne+$numberTwo;
       }
       if($operationType=="minus")
       {
           $result=$numberOne-$numberTwo;
       }
       if($operationType=="multiplication")
       {
           $result=$numberOne*$numberTwo;
       }
       if($operationType=="division")
       {
           $result=$numberOne/$numberTwo;
       }
       if($operationType=="modulus")
       {
           $result=$numberOne%$numberTwo;
       }
    }

?>

<form action="" method="post">
    <fieldset style="width:20%;">
        <legend>Calculator Design:</legend>
                <div>
                    <label for="number_1">Number One:</label>
                    <input type="number" name="number_1" id="number_1">
                </div>

                <div style="margin-top:10px;">
                    <label for="number_2">Number Two:</label>
                    <input type="number" name="number_2" id="number_2">
                </div>

                <div style="margin-top:10px;">
                    <label for="operation">Operation:</label>
                    <select name="operator">
                            <option  value="plus">+</option>
                            <option  value="minus">-</option>
                            <option  value="multiplication">*</option>
                            <option  value="division">/</option>
                            <option  value="modulus">%</option>
                    </select>
                </div>

                <div style="margin-top:10px;">
                    <button type="submit">Calculate</button>
                </div>
    </fieldset>
</form>
<div style="background-color:gray;color:white;width:280px;height:250px;box-shadow:1px 1px black;">
        <h3>Result is : <?php echo $result ?? '' ?></h3>
</div>