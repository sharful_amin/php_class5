<?php









$data = array(1, 2, 'Prabir', true, 5);

$arr = [1, 2, 3, 4];

echo "<pre>";
    var_dump($arr);
echo "</pre>";

echo "<br>";

$students = [
        'Prabir',
        'Karim',
        'Rahim',
        false,
        500
];

echo "<pre>";
    print_r($students);

    echo $students[1];
    echo $students[2];
echo "</pre>";

echo '<br>';
echo '<br>';
echo '<br>';

$person = [];

var_dump($person);

echo "<br>";
echo "<br>";
echo "<br>";




$person = [
    'name' => 'Prabir',
    'location' => 'Dhaka',
    'phone_number' => '175269',
    'is_active' => true
];

$person['extra_kore_delam'] = 'Extra';
$person[] = 100;
$person[] = 200;

echo "<pre>";
    print_r($person);

    echo $person['phone_number'];
    echo "<br>";
    echo "<br>";
    echo $person['location'];
echo "</pre>";



echo "<br>";
echo "<br>";
echo "<br>";

$skills = [
    'HTML' => [
        'v1' => 'HTML 4',
        'v2' => 'HTML 5',
    ],
    'CSS'=> [
        1,2,3,4,5
    ],
    'PHP'

];

echo $skills['CSS'][3];

echo "<pre>";
    print_r($skills);

    echo $skills['HTML']['v1'];
    echo "<br>";
    echo $skills['HTML']['v2'];
echo "</pre>";









?>